
# Boitier Camera ESP32-CAM

Boitier 87x60x44 mm acceptant un capteur 3 en 1 (co2, température, humidtité) et 3 bandeaux led adressable. Ajout facile avec l'integration esphome de Home Assistant (09:45): 





## Composants
Liens amazon prime (Total ≃ 8€):
- [Aimants](https://amzn.to/3BYryyv)


Liens aliexpress (Total ≃ 31€):
- [Capteur SD40](https://fr.aliexpress.com/item/1005004221715765.html)
- [Bandeau WS2812b (144/m)](https://fr.aliexpress.com/item/2036819167.html?UTABTest=aliabtest359606_508211&aff_fcid=f70ffdaeb0074f95bbbd118f6b4938fb-1671880749238-07460-_DdZ5urd&tt=CPS_NORMAL&aff_fsk=_DdZ5urd&aff_platform=shareComponent-detail&sk=_DdZ5urd&aff_trace_key=f70ffdaeb0074f95bbbd118f6b4938fb-1671880749238-07460-_DdZ5urd&terminal_id=fd3a99d8b9ca4abfa866a70786b7e17e&OLP=1084900408_f_group0&o_s_id=1084900408&afSmartRedirect=y)
- [D1 mini](https://s.click.aliexpress.com/e/_DF7rKex)




## Connections 

- SD40

```bash
  -> 3.3V
  -> GND
  -> D1 (SCL)
  -> D2 (SDA)
```
- WS2812b x 3 (en parallèle)

```bash
  -> 5V
  -> GND
  -> D5 
  -> D6
  -> D7
```
## Filaments

| Couleur            | Hex                                                               | Filament                                                           |
| ----------------- | ------------------------------------------------------------------ | ------------------------------------------------------------------ |
| Boitier| ![#ffffff](https://via.placeholder.com/10/ffffff?text=+)| [Marbre](https://www.wanhaofrance.com/products/pla-premium-marbre-1kg-1-75-ender3-creality-wanhao?ref=abrege) 
| Face avant | ![#0a192f](https://via.placeholder.com/10/0a192f?text=+) | [Noir mat](https://amzn.to/3jr6tGO)
| Diffuseur | ![#ffffff](https://via.placeholder.com/10/ffffff?text=+) | [Blanc](https://www.filimprimante3d.fr/filament-pla-175-mm/3245-filament-premium-pla-175mm-blanc-arctique-1kg-spectrum.html)



## Auteurs

- [@tsoriano_geneva](https://gitlab.com/tsoriano_geneva)

